#Send current JustOn contract to subscribers to inform them about their current subscription status
##Concept:

###Subscriptions

Die Kunden wollen wissen, wann Ablauf  Ihres Vertrags ist und wie viele Lizenzen von Typ Full Access und von Typ Read Only sind.

Aus JustOn-Sicht passiert das zur Zeit:
- Wir loggen uns in unseren SF Org ein
- Wir suchen die Subscription
- Wir erstellen händisch ein Zusammenfassung der Lizenzen
- Pro Aktiv Kunde Kommunikation

###Wunschvorstellung
JustOn würde gern nicht nur automatisch sondern auch zu jederzeit in der Lage sein, den Vertragsstand alle Subscribers in den SF Org sichtbar und per E-Mail an die Kunden zu kommunizieren. Einen Pdf als Anhang kann per E-Mail an der Contact von Account versendet werden. 

###Create PDF und Send Email

Deswegen gibt es für manual Email in bestimmte Subscription ein Create PDF button und ein Send Email button. JustOn kann erstmal ein Pdf von Subscription mit Klicken auf  Create PDF button sehen.
 
**Hinweis:** Auf das PDF steht folgenden infos von Item : Title, Description, Billing Type, Quantity, Price, Startdatum.

Dann schickt das PDF als Anhang per Email mit Klicken auf **Send Email** button. 

###Schedule Email

Automatische Nachricht über den aktuellen Vertragsstand wird 1 Monat vor Ablauf der Kündigungsfrist an der Kunde verschickt.

##Operation:

###Managing Subscription Items

**manual send Email**

**1.Send Email**
- Öffnen Sie **Subscription**
- klicken Sie auf **bestimmte Subscription**
- Klicken Sie auf **Create PDF**

**1.Send Email**
- Öffnen Sie **Subscription**
- klicken Sie auf **bestimmte Subscription**
- Klicken Sie auf **Send Email**

#Administration:

##Create PDF and Send Email- Button in Layouts
- öffnen Sie **Setup**
- Klicken Sie auf **Object Manager**
- Suchen Sie **Subscription**
- Klicken Sie **Page Layouts** 
- Klicken Sie **Subscription Layout**
- Klicken Sie **Buttons**
- Finden Sie **Create PDF** und **Send Email**

##Custom Labels for PDF
Sie haben für PDF in Visualforce Page ( **ViewSubPDF.vfp** ) ein Paar Labels und Master Labels ist Englisch  und das übersetz Deutsch:
- Öffnen Sie **Setup**
- Schreiben Sie in Quick Find **Custom Labels**
- Finden Sie **Subscription, SubscriptionName, StartDate, EndDate, Item**

##Custom Labels for Email
Sie haben für Email in Apex Klasse ( **DetailSubsItem.apxc** ) Labels für Body und Subject. Master Labels ist Englisch  und das übersetz Deutsch:
- Öffnen Sie **Setup
- Schreiben Sie in Quick Find **Custom Labels**
- Finden Sie **SubjectEmail, BodyEmail, AccountLabel**

##Create PDF and Send Email- Button 
Für Send Email Button und Create PDF Button erwähnt **SendEmail.vfp** und **ViewSubPDF.vfp**
- Öffnen Sie **Setup**
- Klicken Sie auf **Object Manager**
- Suchen Sie **Subscription**
- Klicken Sie **Buttons,Links and Actions** 
- Finden Sie **Create PDF** und **Send Email** 

##Schedule Email
Wenn EndDate bestimmter Subscription in **ein einem Monat** beendet wird, wird automatische Nachricht per E-Mail an der Contact versendet. Das erwähnt **SubscriptionEmailsScheduler.apxc** apex klasse.
- Öffnen Sie **Setup**
- Schreiben Sie in Quick Find **Scheduled Jobs**
- Finden Sie **Send Email Schedule**




