global class SubscriptionEmailsScheduler Implements Schedulable{
    
   @TestVisible private static Date inOneMonth = date.today().addMonths(1);
   @testvisible private String addresses;
   @testvisible private String[] toAddresses;
   @testvisible private Map<Id, List<String>> mappedToAddresses = new Map<Id, List<String>>(); // just for tests ... 
    
   global void execute(SchedulableContext sc)
   {
       
       sendmail();
   }

   public void sendmail()
    {
        
        Map<Id, ONB2__Subscription__c> subs = new Map<Id, ONB2__Subscription__c>([SELECT Id, ONB2__Account__c, Name, ONB2__RenewalDate__c, ONB2__Status__c 
                                                                                  FROM ONB2__Subscription__c 
                                                                                  WHERE (ONB2__RenewalDate__c != null) AND (ONB2__Status__c ='Active')]);
        
        Map<Id, List<ONB2__Subscription__c>> mappedAccountSubscriptions = new Map<Id, List<ONB2__Subscription__c>>();
        
        for( ONB2__Subscription__c sub : subs.values() ) {
            
            List<ONB2__Subscription__c> subscriptionsFromAccount = mappedAccountSubscriptions.get(sub.ONB2__Account__c);
            
            if ( subscriptionsFromAccount == null ) {
                subscriptionsFromAccount = new List<ONB2__Subscription__c>();   
            }
        
            subscriptionsFromAccount.add(sub);
        
            mappedAccountSubscriptions.put( sub.ONB2__Account__c, subscriptionsFromAccount );
        }
        
        Set<Id> accIds = mappedAccountSubscriptions.keySet();
        
        List<Account> accounts = [SELECT Id, Name, (SELECT Id, Email FROM Contacts) FROM Account WHERE Id iN :accIds];
        
        for ( Account account : accounts ) {
            
            List<ONB2__Subscription__c> accountSubscriptions = mappedAccountSubscriptions.get(account.Id);

        	for ( ONB2__Subscription__c subscription : accountSubscriptions ){
                            
           	    if ( subscription.ONB2__RenewalDate__c ==  inOneMonth ){
                                    
                  // Create PDF
                  PageReference pdf = Page.ViewSubpdf;
                  pdf.getParameters().put('id', subscription.Id);
                  Blob b;
                  try {
                     b = !Test.isRunningTest()
                     ? pdf.getContentAsPdf()
                     : Blob.valueOf('Fake PDF content');
                  }
                  catch (Exception e) {
                     ApexPages.addmessage( new ApexPages.message(ApexPages.severity.FATAL,'Failed to generate PDF!') );
                  }
         
                 //Emails of Contact
                 List<Contact> contacts = account.Contacts;
                 
                 if(contacts[0].Email !=null) {
                      addresses = contacts[0].Email;
                      for (Integer i = 1; i < contacts.size(); i++){
                          if(contacts[i].Email != null) {
                          addresses += ':' + contacts[i].Email;
                          }
                      }
                 }
   
                 //create Emails
                 toAddresses = addresses.split(':', 0);
                 mappedToAddresses.put(account.Id, toAddresses);
                 Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                 message.setToAddresses(toAddresses);
                 message.setSubject(System.Label.SubjectLable + ' ' + account.Name );
                 message.setHtmlBody(String.format(System.Label.BodyEmail, new List<String>{account.Name}));
         
                 // Attach PDF to email and send
                 Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                 attachment.setContentType('application/pdf');
                 attachment.setFileName('SubscriptionSummary-' + account.Name + '.pdf');
                 attachment.setInline(false);
                 attachment.setBody(b);
                 message.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
         
                //send Email
                Messaging.sendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ message }); 
              }
         }            
     }
   }
}