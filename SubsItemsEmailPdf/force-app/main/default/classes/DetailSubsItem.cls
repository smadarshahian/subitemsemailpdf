public class DetailSubsItem {
    
    public Id subscriptionId;
    
    public ONB2__Subscription__c subs {
        get { return [SELECT Id, ONB2__Account__c, Name, ONB2__EndDate__c, ONB2__RenewalDate__c
                      FROM ONB2__Subscription__c 
                      WHERE ONB2__Status__c = 'Active' AND id = :subscriptionId ];}
        }
    
    public DetailSubsItem(ApexPages.StandardController stdController) { 
       this.subscriptionId = (Id) stdController.getRecord().Id;
    }
    
    public Account acc {
        get{ return [SELECT id, Name, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry 
                     FROM Account 
                     WHERE id= : subs.ONB2__Account__c];}    
    }
    
    //List Emails of Contact
    public  List<Contact> cont {
        get{return [SELECT id, Email FROM Contact WHERE AccountId=: acc.Id ];} 
    }  
    
    //summary of Items
    public List<ONB2__Item__c> getItems(){ 
        return [SELECT Id, ONB2__Title__c, ONB2__Description__c, ONB2__Quantity__c, ONB2__BillingType__c, ONB2__Price__c, ONB2__StartDate__c, ONB2__EndDate__c
                FROM ONB2__Item__c
                WHERE ( ONB2__Subscription__c =: subs.Id ) AND ( ONB2__Active__c = true ) AND ( ONB2__BillingType__c = 'Recurring') 
               ];    
    }
    
     @testvisible private String addresses;
     @testvisible private String[] toAddresses;
    
     public PageReference sendEmail() {
         // Create PDF
        PageReference pdf = Page.ViewSubpdf;
        pdf.getParameters().put('id', this.subscriptionId);
        Blob b;
        try {
            b = !Test.isRunningTest()
            ? pdf.getContentAsPdf()
            : Blob.valueOf('Fake PDF content');
        }
        catch (Exception e) {
           
            ApexPages.addmessage( new ApexPages.message(ApexPages.severity.FATAL,'Failed to generate PDF!') );
            return null;
        }
         
         //Emails of Contact
         
         if(this.Cont[0].Email !=null) {
             addresses = this.Cont[0].Email;
             for (Integer i = 1; i < this.Cont.size(); i++){
                 if(this.Cont[i].Email != null) {
                     addresses += ':' + this.Cont[i].Email;
                 }
             }
         }
         
         //create Emails
         toAddresses = addresses.split(':', 0);
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         message.setToAddresses(toAddresses);
         message.setSubject(System.Label.SubjectEmail + ' ' + acc.Name);
         message.setHtmlBody(String.format(System.Label.BodyEmail, new List<String>{acc.Name}));
         
        // Attach PDF to email and send
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setContentType('application/pdf');
        attachment.setFileName('SubscriptionSummary-' + acc.Name + '.pdf');
        attachment.setInline(false);
        attachment.setBody(b);
        message.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
         
        //send Email
        Messaging.sendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ message }); 
         
        return null;
     }
}